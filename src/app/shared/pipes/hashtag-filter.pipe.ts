import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hashtagFilter'
})
export class HashtagFilterPipe implements PipeTransform {

  transform(value: string[]): string[] {
    if (value.length > 2) {
      return value.slice(0, 1);
    } else {
      return value;
    }
  }

}
