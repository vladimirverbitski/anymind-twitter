import {User} from './user';

export interface Tweet {
  account: User;
  date: string;
  hashtags: Array<string>;
  likes: number;
  replies: number;
  retweets: number;
  text: string;
}
