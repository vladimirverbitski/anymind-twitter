import {Tweet} from './tweet';

export interface TweetResponse {
  count: number;
  offset: number;
  results: Array<Tweet>;
}
