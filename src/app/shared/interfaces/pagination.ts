export interface Pagination {
  offset: number;
  hasNext: boolean;
}
