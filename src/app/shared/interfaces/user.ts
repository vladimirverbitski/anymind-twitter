export interface User {
  fullname: string;
  href: string;
  id: number;
}
