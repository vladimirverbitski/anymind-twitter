import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Pagination} from '../../interfaces/pagination';

@Component({
  selector: 'app-paggination',
  templateUrl: './paggination.component.html',
  styleUrls: ['./paggination.component.css']
})
export class PagginationComponent implements OnInit {

  @Output() public getPageFunction = new EventEmitter();
  @Input() public paginationInfo: Pagination;

  constructor() { }

  ngOnInit() {
  }

  getPage(page: number) {
    if (page) {
      this.getPageFunction.emit(page);
    } else {
      this.getPageFunction.emit(0);
    }
  }

}
