import {Component, Input, OnInit} from '@angular/core';
import {Tweet} from '../../interfaces/tweet';

@Component({
  selector: 'app-search-table',
  templateUrl: './search-table.component.html',
  styleUrls: ['./search-table.component.css']
})
export class SearchTableComponent implements OnInit {

  @Input() tweets: Array<Tweet>;
  @Input() spinner;

  constructor() { }

  ngOnInit() {
  }

}
