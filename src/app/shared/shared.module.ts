import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchTableComponent} from './components/search-table/search-table.component';
import {TrimTextPipe} from './pipes/trim-text.pipe';
import {HashtagFilterPipe} from './pipes/hashtag-filter.pipe';
import {FormsModule} from '@angular/forms';
import { PagginationComponent } from './components/paggination/paggination.component';

@NgModule({
  declarations: [
    SearchTableComponent,
    TrimTextPipe,
    HashtagFilterPipe,
    PagginationComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    SearchTableComponent,
    TrimTextPipe,
    HashtagFilterPipe,
    PagginationComponent
  ]
})
export class SharedModule { }
