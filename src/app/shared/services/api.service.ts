import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {TweetResponse} from '../interfaces/tweet-response';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl = 'https://anymind-recruitment-python-backend.adasia.biz';

  constructor(
    private http: HttpClient
  ) { }

  getTweets(searchBy: string, query: string, offset: number): Observable<TweetResponse> {
    return this.http.get<TweetResponse>(`${this.apiUrl}/${searchBy}/${query}?offset=${offset}`)
      .pipe(
        map(res => res)
      );
  }

}
