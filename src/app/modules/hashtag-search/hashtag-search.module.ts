import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HashtagSearchComponent } from './search/hashtag-search.component';
import {HashtagRoutingModule} from './hashtag-routing.module';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [
    HashtagSearchComponent
  ],
  imports: [
    CommonModule,
    HashtagRoutingModule,
    FormsModule,
    SharedModule,
  ],
  entryComponents: []
})
export class HashtagSearchModule { }
