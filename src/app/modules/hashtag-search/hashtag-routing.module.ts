import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HashtagSearchComponent} from './search/hashtag-search.component';

const routes: Routes = [
  {
    path: '',
    component: HashtagSearchComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HashtagRoutingModule {}
