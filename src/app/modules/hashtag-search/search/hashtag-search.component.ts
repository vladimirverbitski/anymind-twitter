import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from '../../../shared/services/api.service';
import {Tweet} from '../../../shared/interfaces/tweet';
import {takeUntil} from 'rxjs/operators';
import {forkJoin, Subject} from 'rxjs';
import {Pagination} from '../../../shared/interfaces/pagination';

@Component({
  selector: 'app-hashtag-search',
  templateUrl: './hashtag-search.component.html',
  styleUrls: ['./hashtag-search.component.css']
})
export class HashtagSearchComponent implements OnInit, OnDestroy {

  searchInput: string;
  tweets: Array<Tweet> = [];
  unsubscribe: Subject<void> = new Subject();
  isLoading = false;
  searchQuery: Pagination = {
    offset: 0,
    hasNext: false
  };

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
  }

  search() {
    this.getPage(0);
  }

  getPage(offset: number) {
    this.searchQuery.offset = offset;
    this.isLoading = true;
    forkJoin([
      this.api.getTweets('hashtags', this.searchInput, this.searchQuery.offset),
      this.api.getTweets('hashtags', this.searchInput, this.searchQuery.offset + 1)
    ]).pipe(
        takeUntil(this.unsubscribe)
      )
      .subscribe(it => {
        this.isLoading = false;
        this.tweets = it[0].results;
        this.searchQuery.offset = it[0].offset;
        this.searchQuery.hasNext = !!it[1].results;
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
