import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSearchComponent } from './search/user-search.component';
import {FormsModule} from '@angular/forms';
import {UserRoutingModule} from './user-routing.module';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  declarations: [
    UserSearchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule,
    SharedModule,
  ]
})
export class UserSearchModule { }
