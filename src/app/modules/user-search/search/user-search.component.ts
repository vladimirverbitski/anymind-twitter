import {Component, OnDestroy, OnInit} from '@angular/core';
import {Tweet} from '../../../shared/interfaces/tweet';
import {forkJoin, Subject} from 'rxjs';
import {ApiService} from '../../../shared/services/api.service';
import {takeUntil} from 'rxjs/operators';
import {Pagination} from '../../../shared/interfaces/pagination';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.css']
})
export class UserSearchComponent implements OnInit, OnDestroy {

  searchInput: string;
  tweets: Array<Tweet> = [];
  unsubscribe: Subject<void> = new Subject();
  isLoading = false;
  searchQuery: Pagination = {
    offset: 0,
    hasNext: false
  };

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
  }

  search() {
    this.getPage(0);
  }

  getPage(offset: number) {
    this.searchQuery.offset = offset;
    this.isLoading = true;
    forkJoin([
      this.api.getTweets('users', this.searchInput, this.searchQuery.offset),
      this.api.getTweets('users', this.searchInput, this.searchQuery.offset + 1)
    ])
      .pipe(
        takeUntil(this.unsubscribe)
      )
      .subscribe(it => {
        this.isLoading = false;
        this.tweets = it[0].results;
        this.searchQuery.offset = it[0].offset;
        this.searchQuery.hasNext = it[1].results.length > 0 ;
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

}
