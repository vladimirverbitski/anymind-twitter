import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HashtagSearchModule} from './modules/hashtag-search/hashtag-search.module';
import {UserSearchModule} from './modules/user-search/user-search.module';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    HashtagSearchModule,
    UserSearchModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
