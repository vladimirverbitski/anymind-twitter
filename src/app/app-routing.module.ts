import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'hashtag',
    pathMatch: 'full'
  },
  {
    path: 'hashtag',
    loadChildren: () => import('./modules/hashtag-search/hashtag-search.module').then(m => m.HashtagSearchModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./modules/user-search/user-search.module').then(m => m.UserSearchModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
